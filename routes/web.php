<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('test');

Route::post('/cart/add', 'CartController@create')->name('cart.add');
Route::patch('/cart/update/{id}', 'CartController@update')->name('cart.update');
Route::delete('/cart/delete/{id}', 'CartController@delete')->name('cart.delete');
Route::get('/cart', 'CartController@show')->name('cart.index');

Route::get('/checkout', 'CheckoutController@index')->name('checkout');
Route::post('/checkout', 'CheckoutController@store')->name('checkout.store');

Route::get('/p/{slug}', 'ProductController@show');
Route::get('/c/{slug?}', 'CategoryController@show')->name('category.menu');

Route::post('/r/comments', 'CommentController@store')->name('comments.store');

Route::middleware('auth')->group(function () {
    Route::get('/u/profile', 'UserController@edit');
    Route::patch('/u/profile', 'UserController@update')->name('user.update');
});

Route::group(['middleware' => ['role:admin'], 'prefix' => 'admin'], function () {
    Route::get('/', 'Admin\AdminController@index')->name('admin.index');
    Route::resource('roles','Admin\RoleController');
    Route::resource('permissions', 'Admin\PermissionController')->except(['show']);
    Route::resource('users', 'Admin\UserController');
    Route::resource('categories', 'Admin\CategoryController')->except(['show']);
    Route::resource('products', 'Admin\ProductController');
    Route::resource('download', 'Admin\DownloadController')->only(['store', 'destroy']);
    Route::resource('deliveries', 'Admin\DeliveryController')->except(['show']);
    Route::resource('orders', 'Admin\OrderController')->except(['create', 'store']);
    Route::resource('comments', 'Admin\CommentController')->except(['create']);
});
