<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function show($slug)
    {
        $category = Category::with('products')->where('slug', $slug)->firstOrFail();
        $products = $category->products()->paginate(20);

        return view('front.categories.index', [
          'category' => $category,
          'products' => $products
        ]);
    }
}
