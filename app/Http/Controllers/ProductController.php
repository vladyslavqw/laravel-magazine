<?php

namespace App\Http\Controllers;

use App\Product;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
class ProductController extends Controller
{
    public function show($slug)
    {
      $product = Product::with('images')->where('slug', $slug)->firstOrFail();
      $comments = $product->comments()->paginate(10);

      return view('front.products.show', [
        'product' => $product,
        'comments' => $comments
      ]);
    }
}
