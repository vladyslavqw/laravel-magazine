<?php

namespace App\Http\Controllers;

use Cart;
use App\Order;
use App\User;
use Illuminate\Http\Request;
use Stripe\Error\Card;
use Cartalyst\Stripe\Stripe;
use Illuminate\Support\Facades\Auth;

class CheckoutController extends Controller
{
  public function index()
  {
      return view('front.checkout.index', [
        'user' => User::find(Auth::id())
      ]);
  }

  public function store(Request $request)
  {
    $this->validate($request, [
      'name'=> 'required|string',
      'email' => 'required|email',
      'address' => 'required',
      'city' => 'required|string',
      'province' => 'required|string',
      'postalcode' => 'required|numeric',
      'phone' => 'required|numeric',
      'card-number' => 'required|numeric',
      'exp_mm'=> 'required|numeric',
      'cvc' => 'required|numeric',
      'exp_yy' => 'required|numeric'
    ]);

    $stripe = Stripe::make(config('services.stripe.secret'));

    try {
      $token = $stripe->tokens()->create([
        'card' => [
          'number' => $request->input('card-number'),
          'exp_month'=> $request->input('exp_mm'),
          'cvc' => $request->input('cvc'),
          'exp_year' => $request->input('exp_yy')
        ],
      ]);

      $charge = $stripe->charges()->create([
        'card' => $token['id'],
        'currency' => 'USD',
        'amount' => Cart::total(),
        'description' => ''
      ]);

      $order = Order::create([
        'user_id' => Auth::id(),
        'name'=> $request->name,
        'email' => $request->email,
        'address' => $request->address,
        'city' => $request->city,
        'province' => $request->province,
        'postalcode' => $request->postalcode,
        'phone' => $request->phone,
        'amount' => Cart::total(2, '.',''),
        'order_status' => 'Awating shipment'

      ]);

      $orderItems = Cart::content()->mapWithKeys(function ($item) {
        return [$item->id => ['name'=> $item->name, 'qty'=> $item->qty, 'price' => $item->price]];
      })->toArray();

      $order->orderItems()->createMany($orderItems);
      $order->save();

      Cart::destroy();

      return redirect()->route('test');

    }  catch (\Exception $e) {

      return back()->withErrors($e->getMessage());

    } catch(\Cartalyst\Stripe\Exception\CardErrorException $e) {

      return back()->withErrors($e->getMessage());

    } catch(\Cartalyst\Stripe\Exception\BadRequestException $e) {

      return back()->withError($e->getMessage());

    }





  }
}
