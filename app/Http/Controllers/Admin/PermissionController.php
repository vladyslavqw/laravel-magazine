<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Permission;
use App\DataTables\PermissionDataTable;

class PermissionController extends Controller
{
  public function __construct()
  {
    $this->middleware('permission:permission-list');
    $this->middleware('permission:permission-create', ['only' => ['create','store']]);
    $this->middleware('permission:permission-edit', ['only' => ['edit','update']]);
    $this->middleware('permission:permission-delete', ['only' => ['destroy']]);
  }

  public function index(PermissionDataTable $dataTable)
  {
    return $dataTable->render('admin.permissions.index');
  }

  public function create()
  {
    return view('admin.permissions.create');
  }

  public function store(Request $request)
  {
    $this->validate($request, [
            'name'=>'required|unique:permissions,name',
        ]);

    Permission::create(['name' => $request->input('name')]);

    return redirect()->route('permissions.index')
                    ->with('success','Permission created successfully');
  }

  public function edit(Permission $permission)
  {
    return view('admin.permissions.edit',[
      'permission' => $permission
    ]);
  }

  public function update(Request $request, Permission $permission)
  {
    $this->validate($request, [
            'name'=>'required|unique:permissions,name',
        ]);

    $permission->update(['name' => $request->input('name')]);
    $permission->save();

    return redirect()->route('permissions.index')
                    ->with('success', 'Permission updated successfully');
  }

  public function destroy(Permission $permission)
  {
    $permission->delete();
    return redirect()->route('permissions.index')
                    ->with('success','Permission deleted successfully');
  }

}
