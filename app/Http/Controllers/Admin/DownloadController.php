<?php

namespace App\Http\Controllers\Admin;

use App\Download;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class DownloadController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
          'file' => 'image'
        ]);

        $path = $request->file->store('uploads', 'public');

        $download = Download::create([
          'title' => $request->title,
          'path' => $path,
          'mime' => $request->file->getMimeType(),
          'size' => $request->file->getSize()
        ]);

        return $download->id;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Download $download)
    {
        if (Storage::disk('public')->delete($download->path)) {
          return;
        }

        if ($download->delete()) {

          if ($download->products()) {
            $download->products()->detach();
            return ['result' => true];
          }
          else {
            return ['result' => true];
          }


        }
    }
}
