<?php

namespace App\Http\Controllers\Admin;

use App\Product;
use App\Category;
use App\Delivery;
use App\DataTables\ProductDataTable;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProductController extends Controller
{
  public function __construct()
  {
    $this->middleware('permission:product-list');
    $this->middleware('permission:product-create', ['only' => ['create','store']]);
    $this->middleware('permission:product-edit', ['only' => ['edit','update']]);
    $this->middleware('permission:product-delete', ['only' => ['destroy']]);
  }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ProductDataTable $datatable)
    {
        return $datatable->render('admin.products.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.products.create', [
          'product' => [],
          'categories' => Category::with('children')->where('parent_id', 0)->get(),
          'delimiter' => '',
          'deliveries' => Delivery::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
          'sku' => 'required',
          'title' => 'required|max:40',
          'categories' => 'required',
          'description' => 'required',
          'qty' => 'required|min:1',
          'price' => 'required',
          'published' => 'required'
        ]);
        $product = Product::create($request->only('sku', 'title', 'description', 'qty', 'price', 'published'));

        if ($request->has('categories')) {
          $product->categories()->attach($request->input('categories'));
        }

        if ($request->has('deliveries')) {
          $product->deliveries()->attach($request->input('deliveries'));
        }

        if ($request->has('downloads')) {
          $product->images()->attach($request->downloads);
        }

        return redirect()->route('products.index')
                        ->with('success','Products created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        return view('admin.products.show', [
          'product' => $product
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
      return view('admin.products.edit', [
        'product' => $product,
        'categories' => category::with('children')->where('parent_id', 0)->get(),
        'delimiter' => '',
        'deliveries' => Delivery::all()
      ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $this->validate($request,[
          'sku' => 'required',
          'title' => 'required|max:40',
          'description' => 'required',
          'qty' => 'required|min:1',
          'price' => 'required',
          'published' => 'required'
        ]);
        $product->update($request->except('categories', 'downloads', 'deliveries'));

        $product->categories()->sync($request->input('categories'));
        $product->deliveries()->sync($request->input('deliveries'));
        $product->images()->sync($request->input('downloads'));

        return redirect()->route('products.index')
                        ->with('success','Product updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product->categories()->detach();
        $product->deliveries()->detach();
        $product->images()->detach();
        $product->delete();
        return redirect()->route('products.index')
                        ->with('success','Product deleted successfully');
    }
}
