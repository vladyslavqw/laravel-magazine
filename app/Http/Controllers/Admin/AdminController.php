<?php

namespace App\Http\Controllers\Admin;

use App\Order;
use App\User;
use App\Product;
use App\Comment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Analytics;
use Spatie\Analytics\Period;

class AdminController extends Controller
{
    public function index() {
      $analyticsData = Analytics::fetchUserTypes(Period::days(7));
      $countries = Analytics::performQuery(
        Period::days(7),
        'ga:visits',
        [
          'dimensions' => 'ga:country',
          'sort' => '-ga:visits'
        ]
      )->rows;
       $timepages = Analytics::performQuery(
        Period::days(7),
        'ga:timeOnPage',
        ['dimensions' => 'ga:pagePath', 'sort' => '-ga:timeOnPage', 'max-results' =>'20'])->rows;
        $operatingsystems = Analytics::performQuery(
         Period::days(7),
         'ga:visits',
         ['dimensions' => 'ga:operatingSystem', 'sort' => '-ga:visits', 'max-results' => '20'])->rows;
      // dd($operatingsystems);
      return view('admin.index', [
        'browsers' => Analytics::fetchTopBrowsers(Period::days(14))->toArray(),
        'countries' => $countries,
        'visitspages' => Analytics::fetchVisitorsAndPageViews(Period::months(6))->toArray(),
        'mostVisitedPages' => Analytics::fetchMostVisitedPages(Period::days(7), '20')->toArray(),
        'timepages' => $timepages,
        'operatingsystems' => $operatingsystems,
        'users' => User::count(),
        'orders' => Order::count(),
        'products' => Product::count(),
        'comments' => Comment::count()
      ]);
    }
}
