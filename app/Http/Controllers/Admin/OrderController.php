<?php

namespace App\Http\Controllers\Admin;

use App\Order;
use App\DataTables\OrderDataTable;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrderController extends Controller
{
    public function __construct()
    {
      $this->middleware('permission:order-list');
      $this->middleware('permission:order-edit', ['only' => ['edit','update']]);
      $this->middleware('permission:order-delete', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(OrderDataTable $datatable)
    {
        return $datatable->render('admin.orders.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AppOrder  $appOrder
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
      // $order1 = Order::find(2);
      // $tests = $order1->orderItems;
      // foreach ($tests as $item) {
      //   echo $item->name;
      // }
        return view('admin.orders.show', [
          'order' => $order
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AppOrder  $appOrder
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        return view('admin.orders.edit', [
          'order' => $order
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AppOrder  $appOrder
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        $order->update($request->except('amount'));
        $order->save;

        return redirect()->route('orders.index')->with('success', 'Order updated susseccfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AppOrder  $appOrder
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        $order->delete();

        return redirect()->route('orders.index')->with('success', 'Order deleted successfully');
    }

}
