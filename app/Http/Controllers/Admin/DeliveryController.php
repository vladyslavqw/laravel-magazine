<?php

namespace App\Http\Controllers\Admin;

use App\Delivery;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\DataTables\DeliveryDataTable;

class DeliveryController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:download-list');
        $this->middleware('permission:download-create', ['only' => ['store']]);
        $this->middleware('permission:download-delete', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(DeliveryDataTable $datatable)
    {
        return $datatable->render('admin.deliveries.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.deliveries.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required'
        ]);

        Delivery::create($request->all());

        return redirect()->route('deliveries.index')->with('success', 'Delivery added successfully');
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Delivery  $delivery
     * @return \Illuminate\Http\Response
     */
    public function edit(Delivery $delivery)
    {
        return view('admin.deliveries.edit', [
            'deliveries' => $delivery
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Delivery  $delivery
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Delivery $delivery)
    {
        $this->validate($request, [
            'title' => 'required'
        ]);

        $delivery->update($request->all());

        return redirect()->route('deliveries.index')->with('success', 'Delivery updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Delivery  $delivery
     * @return \Illuminate\Http\Response
     */
    public function destroy(Delivery $delivery)
    {
        $delivery->products()->detach();
        $delivery->delete();

        return redirect()->route('deliveries.index')->with('success', 'Delivery deleted successfully');
    }
}
