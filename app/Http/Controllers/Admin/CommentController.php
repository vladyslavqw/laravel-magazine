<?php

namespace App\Http\Controllers\Admin;

use App\Comment;
use Illuminate\Http\Request;
use App\DataTables\CommentDataTable;
use App\Http\Controllers\Controller;

class CommentController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:comment-list');
        $this->middleware('permission:comment-edit', ['only' => ['edit, update']]);
        $this->middleware('permission:comment-delete', ['only' => ['destroy']]);
    }

    public function index(CommentDataTable $datatable)
    {
        return $datatable->render('admin.comments.index');
    }

    public function show(Comment $comment)
    {
      return view('admin.comments.show', [
        'comment' => $comment
      ]);
    }

    public function edit(Comment $comment)
    {
      return view('admin.comments.edit', [
        'comment' => $comment
      ]);
    }

    public function update(Request $requset, Comment $comment)
    {
      $this->validate($requset, [
        'name' => 'required',
        'email' => 'required',
        'body' => 'required'
      ]);

      $comment->update($requset->all());

      return redirect()->route('comments.index')->with('success', 'Comment updated successfully');
    }

    public function destroy(Comment $comment)
    {
      $comment->delete();
      return redirect()->route('comments.index')->with('success', 'Comment deleted successfully');
    }
}
