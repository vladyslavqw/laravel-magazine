<?php

namespace App\Http\Controllers;

use Cart;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CartController extends Controller
{

    public function create(Request $request)
    {
        $product = Product::find($request->input('product_id'));

        Cart::add($product->id, $product->title, 1, $product->price);

        return response('success', 200);
    }

    public function show() {
        return view('front.cart.show');
    }

    public function update($id,Request $request)
    {
        Cart::update($id, $request->quantity);
        return response('success', 200);
    }

    public function delete($id)
    {
        Cart::remove($id);

        return response('success', 200);
    }

}
