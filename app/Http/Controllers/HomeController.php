<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
      $products = Product::with('images')->orderBy('id', 'desc')->where('published', 1)->paginate(20);

        return view('front.index', [
          'products' => $products,
        ]);
    }

    public function show($slug)
    {

      return view('front.products.show', [
        'product' => Product::with('images')->all()
      ]);
    }
}
