<?php

namespace App\Http\Controllers;

use App\Comment;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
          'body' => 'required'
        ]);

        Comment::create([
          'product_id' => $request->product_id,
          'user_id' => Auth::id(),
          'name' => $request->name,
          'email' => $request->email,
          'body' => $request->body
        ]);

        return redirect()->back()->with('success', 'Comment added successfully');
    }

}
