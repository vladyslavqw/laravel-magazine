<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $guarded = ['id', 'created_at'];

    public function orderItems()
    {
      return $this->hasMany('App\OrderItem', 'order_id');
    }
}
