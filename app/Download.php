<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Download extends Model
{
    protected $guarded = [];

    public function products()
    {
        return $this->morphedByMany('App\Product', 'downloadable');
    }
}
