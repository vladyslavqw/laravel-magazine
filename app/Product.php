<?php

namespace App;

use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasSlug;

    protected $fillable = ['sku', 'title', 'description', 'qty', 'price', 'specs', 'published'];

    public function categories()
    {
      return $this->belongsToMany('App\Category');
    }

    public function deliveries()
    {
      return $this->belongsToMany('App\Delivery');
    }

    public function images()
    {
      return $this->morphToMany(Download::class, 'downloadable');
    }

    public function comments()
    {
      return $this->hasMany('App\Comment');
    }

    /**
     * Get the options for generating the slug.
     */
    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('title')
            ->saveSlugsTo('slug');
    }
}
