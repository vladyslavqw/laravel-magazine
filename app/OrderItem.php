<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    protected $guarded = ['id', 'created_at'];

    public function orders()
    {
      return $this->belongsTo('App\Order');
    }
}
