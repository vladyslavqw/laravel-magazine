@extends('adminlte::page')

@section('title', 'Permissions')

@section('content_header')
    <h1>Permissions Management</h1>
@stop

@section('content')
<div class="box">
  <div class="box-header with-border">
    <div class="pull-left">
        <h3 class="box-title">All permissions</h3>
    </div>
    <div class="pull-right">
      <a href="{{route('permissions.create')}}" class="btn btn-primary">Create</a>
    </div>
  </div>
  <div class="box-body">
    @include('messages.list')
    {!! $dataTable->table() !!}

  </div>
</div>

<div class="modal fade" id="ajaxModel" aria-hidden="true">
 <div class="modal-dialog">
     <div class="modal-content">
         <div class="modal-header">
             <h4 class="modal-title" id="modelHeading"></h4>
         </div>
         <div class="modal-body">
             <form id="productForm" name="productForm" class="form-horizontal">
                <input type="hidden" name="product_id" id="product_id">
                 <div class="form-group">
                     <label for="name" class="col-sm-2 control-label">Name</label>
                     <div class="col-sm-12">
                         <input type="text" class="form-control" id="name" name="name" placeholder="Enter Name" value="" maxlength="50" required="">
                     </div>
                 </div>

                 <div class="form-group">
                     <label class="col-sm-2 control-label">Details</label>
                     <div class="col-sm-12">
                         <textarea id="detail" name="detail" required="" placeholder="Enter Details" class="form-control"></textarea>
                     </div>
                 </div>

                 <div class="col-sm-offset-2 col-sm-10">
                  <button type="submit" class="btn btn-primary" id="saveBtn" value="create">Save changes
                  </button>
                 </div>
             </form>
         </div>
     </div>
 </div>
</div>
@stop

@section('js')
{!! $dataTable->scripts() !!}
<script src="https://cdn.datatables.net/buttons/1.0.3/js/dataTables.buttons.min.js"></script>
<script src="/vendor/datatables/buttons.server-side.js"></script>
<script type="text/javascript">

$(document).on('click', '.delete', function(){
        var id = $(this).attr('id');
        if(confirm("Are you sure you want to Delete this data?"))
        {
            $.ajax({
                url:"/permissions/destroy",
                mehtod:"get",
                data:{id:id},
                success:function(data)
                {
                    alert(data);
                    $('#student_table').DataTable().ajax.reload();
                }
            })
        }
        else
        {
            return false;
        }
    });


</script>
@stop
