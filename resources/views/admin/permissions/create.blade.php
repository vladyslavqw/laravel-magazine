@extends('adminlte::page')

@section('title', 'Permissions')

@section('content_header')
    <h1>Permissions Management</h1>
@stop

@section('content')
<div class="box">
  <div class="box-header with-border">
    <div class="pull-left">
        <h3 class="box-title">Create permission</h3>
    </div>
    <div class="pull-right">
      <a href="{{route('permissions.index')}}" class="btn btn-primary">Back</a>
    </div>
  </div>
  <div class="box-body">
    @include('errors.list')
    <form action="{{route('permissions.store')}}" method="post">
      @csrf
      @include('admin.permissions.__form')
    </form>

  </div>
</div>
@stop
