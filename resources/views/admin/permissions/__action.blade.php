<div class="form-group mb-0" role="group">
  @can('permission-edit')
  <a class="btn btn-primary" href="{{ route('permissions.edit', $model->id)}}" role="button">Edit</a>
  @endcan
  @can('permission-delete')
  <form method="POST" action="{{ route('permissions.destroy', $model->id)}}" accept-charset="UTF-8" style="display:inline">
    @csrf
    @method('DELETE')
    <button class="btn btn-danger" type="submit">Delete</button>
  </form>
  @endcan
</div>
