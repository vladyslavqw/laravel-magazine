<div class="form-group">
    <label for="name">Name</label>
    <input name="name" type="text" value="{{$permission->name ?? ''}}" id="name" class="form-control">
</div>

<div class="col-xs-12 col-sm-12 col-md-12 text-center">
    <button class="btn btn-primary" type="submit">Save</button>
</div>
