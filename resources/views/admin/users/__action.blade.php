<div class="form-group" role="group">
  <a class="btn btn-success" href="{{ route('users.show', $model->id)}}" role="button">Show</a>
  @can('user-edit')
  <a class="btn btn-primary" href="{{ route('users.edit', $model->id)}}" role="button">Edit</a>
  @endcan
  @can('user-delete')
  <form method="POST" action="{{ route('users.destroy', $model->id)}}" accept-charset="UTF-8" style="display:inline">
    @csrf
    @method('DELETE')
    <button class="btn btn-danger" type="submit">Delete</button>
  </form>
  @endcan
</div>
