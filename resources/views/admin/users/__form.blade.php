<div class="form-group">
  <label for="name">Name</label>
  <input class="form-control" name="name" type="text" value="{{$user->name ?? ''}}" id="name">
</div>
<div class="form-group">
  <label for="email">Email</label>
  <input class="form-control" name="email" type="email" value="{{$user->email ?? ''}}" id="email">
</div>
<div class='form-group'>
  <label>Roles</label>
  <select class="form-control user-roles" name="roles[]" multiple="multiple" data-placeholder="Select a user roles" style="width: 100%;" tabindex="-1" aria-hidden="true">
    @foreach ($roles as $role)
        <option value="{{$role->id}}" @if( Route::current()->getName() == 'users.edit' ) {{in_array($role->id, $userRoles)?'selected':''}} @endif>{{$role->name}}</option>
    @endforeach
  </select>
</div>
<div class="form-group">
  <label for="password">Password</label><br>
  <input class="form-control" name="password" type="password" value="" id="password">
</div>
<div class="form-group">
  <label for="password">Confirm Password</label><br>
  <input class="form-control" name="password_confirmation" type="password" value="">
</div>

<input class="btn btn-primary" type="submit" value="Save">
