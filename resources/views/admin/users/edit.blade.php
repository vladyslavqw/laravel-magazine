@extends('adminlte::page')
@section('title', 'Users')

@section('content_header')
    <h1>Users Management</h1>
@stop

@section('content')
<div class="box">
  <div class="box-header with-border">
    <div class="pull-left">
        <h3 class="box-title">Edit user</h3>
    </div>
    <div class="pull-right">
        <a class="btn btn-primary" href="{{ route('users.index') }}"> Back</a>
    </div>
  </div>
  <div class="box-body">
    @include('errors.list')
    <form action="{{route('users.update', $user)}}" method="post">
      @csrf
      @method('PUT')
      @include('admin.users.__form')
    </form>
  </div>
</div>


@stop
@section('js')
<script type="text/javascript">
  $(document).ready(function() {
    $('.user-roles').select2();
  });
</script>
@stop
