<div class="form-group mb-0" role="group">
  @can('order-edit')
  <a class="btn btn-success" href="{{ route('orders.show', $model->id)}}" role="button">Show</a>
  @endcan
  @can('order-edit')
  <a class="btn btn-primary" href="{{ route('orders.edit', $model->id)}}" role="button">Edit</a>
  @endcan
  @can('order-delete')
  <form method="POST" action="{{ route('orders.destroy', $model->id)}}" accept-charset="UTF-8" style="display:inline">
    @csrf
    @method('DELETE')
    <button class="btn btn-danger" type="submit">Delete</button>
  </form>
  @endcan
</div>
