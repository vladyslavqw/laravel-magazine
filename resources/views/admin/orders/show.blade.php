@extends('adminlte::page')
@section('title', 'Orders')

@section('content_header')
    <h1>Orders Management</h1>
@stop

@section('content')
<div class="box">
  <div class="box-header with-border">
    <div class="pull-left">
        <h3 class="box-title">Order view</h3>
    </div>
    <div class="pull-right">
        <a class="btn btn-primary" href="{{ route('orders.edit', $order) }}"> Edit</a>
        <a class="btn btn-primary" href="{{ route('orders.index') }}"> Back</a>
    </div>
  </div>
  <div class="box-body">
    <dl class="row">
      <dt class="col-sm-3">Order №</dt>
      <dd class="col-sm-9">{{ $order->id }}</dd>
      <dt class="col-sm-3">Name</dt>
      <dd class="col-sm-9">{{ $order->name }}</dd>
      <dt class="col-sm-3">E-mail</dt>
      <dd class="col-sm-9">{{ $order->email }}</dd>
      <dt class="col-sm-3">Shipping address</dt>
      <dd class="col-sm-9">{{ $order->address }}</dd>
      <dt class="col-sm-3">Phone</dt>
      <dd class="col-sm-9">{{ $order->phone }}</dd>
      <dt class="col-sm-3">City</dt>
      <dd class="col-sm-9">{{ $order->city }}</dd>
      <dt class="col-sm-3">Province</dt>
      <dd class="col-sm-9">{{ $order->province }}</dd>
      <dt class="col-sm-3">Zip code</dt>
      <dd class="col-sm-9">{{ $order->postalcode }}</dd>
      <dt class="col-sm-3">Total amount</dt>
      <dd class="col-sm-9">{{ $order->amount }}</dd>
      <dt class="col-sm-3">Order status</dt>
      <dd class="col-sm-9">{{ $order->order_status }}</dd>
    </dl>
    <table class="table">
      <thead class="thead-light">
        <tr>
          <th scope="col">#</th>
          <th scope="col">Product name</th>
          <th scope="col">Qty</th>
          <th scope="col">Price</th>
        </tr>
      </thead>
      <tbody>
        @foreach($order->orderItems as $item)
        <tr>
          <th scope="row">{{ $loop->iteration }}</th>
          <td>{{ $item->name }}</td>
          <td>{{ $item->qty }}</td>
          <td>{{ $item->price }}</td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</div>
@stop
