@extends('adminlte::page')
@section('title', 'Orders')

@section('content_header')
    <h1>Orders Management</h1>
@stop

@section('content')
<div class="box">
  <div class="box-header with-border">
    <div class="pull-left">
        <h3 class="box-title">Order edit</h3>
    </div>
    <div class="pull-right">
        <a class="btn btn-primary" href="{{ route('orders.edit', $order) }}"> Edit</a>
        <a class="btn btn-primary" href="{{ route('orders.index') }}"> Back</a>
    </div>
  </div>
  <div class="box-body">
    <form action="{{ route('orders.update', $order->id) }}" method="post">
      @csrf
      @method('PUT')
      <div class="form-group">
        <label for="name">Name</label>
        <input type="text" class="form-control" name="name" value="{{ $order->name }}">
      </div>
      <div class="form-group">
        <label for="email">E-mail</label>
        <input type="text" class="form-control" name="email" value="{{ $order->email }}">
      </div>
      <div class="form-group">
        <label for="address">Shipping address</label>
        <input type="text" class="form-control" name="address" value="{{ $order->address }}">
      </div>
      <div class="form-group">
        <label for="phone">Phone</label>
        <input type="text" class="form-control" name="phone" value="{{ $order->phone }}">
      </div>
      <div class="form-group">
        <label for="city">City</label>
        <input type="text" class="form-control" name="city" value="{{ $order->city }}">
      </div>
      <div class="form-group">
        <label for="province">Province</label>
        <input type="text" class="form-control" name="province" value="{{ $order->province }}">
      </div>
      <div class="form-group">
        <label for="postalcode">Zip code</label>
        <input type="text" class="form-control" name="postalcode" value="{{ $order->postalcode }}">
      </div>
      <div class="form-group">
        <label for="order_status">Order status</label>
        <input type="text" class="form-control" name="order_status" value="{{ $order->order_status }}">
      </div>
      <div class="form-group">
        <label for="amount">Amount</label>
        <input type="text" class="form-control" name="amount" value="{{ $order->amount }}" disabled>
      </div>
      <div class="col-12 text-center mb-2">
        <button type="submit" class="btn btn-primary">Save</button>
      </div>
    </form>
    </div>
</div>
@stop
