<div class="form-group mb-0" role="group">
  @can('categories-edit')
  <a class="btn btn-primary" href="{{ route('categories.edit', $model->id)}}" role="button">Edit</a>
  @endcan
  @can('categories-delete')
  <form method="POST" action="{{ route('categories.destroy', $model->id)}}" accept-charset="UTF-8" style="display:inline">
    @csrf
    @method('DELETE')
    <button class="btn btn-danger" type="submit">Delete</button>
  </form>
  @endcan
</div>
