<div class="form-group">
  <label for="name">Name:</label>
  <input type="text" class="form-control" name="name" value="{{$category->name ?? ''}}" placeholder="Enter a name">
</div>
<div class="form-group">
  <label for="parent_id">Parent:</label>
  <select class="form-control" name="parent_id">
    <option value="0">-- Without parent --</option>
    @include('admin.categories.__categories')
  </select>
</div>
<div class="col-xs-12 col-sm-12 col-md-12 text-center">
    <button class="btn btn-primary" type="submit">Save</button>
</div>
