@extends('adminlte::page')
@section('title', 'Categories')

@section('content_header')
    <h1>Categories Management</h1>
@stop

@section('content')
<div class="box">
  <div class="box-header with-border">
    <div class="pull-left">
        <h3 class="box-title">Create category</h3>
    </div>
    <div class="pull-right">
        <a class="btn btn-primary" href="{{ route('categories.index') }}"> Back</a>
    </div>
  </div>
  <div class="box-body">
    @include('errors.list')
    <form action="{{route('categories.store')}}" method="post">
      @csrf
      @include('admin.categories.__form')
    </form>
  </div>
</div>


@stop
