@extends('adminlte::page')

@section('content')
<div class="row">
  <div class="col-md-3 col-sm-6 col-xs-12">
    <div class="info-box">
      <span class="info-box-icon bg-aqua"><i class="fa fa-envelope-o"></i></span>
      <div class="info-box-content">
        <span class="info-box-text">Comments</span>
        <span class="info-box-number">{{ $comments }}</span>
      </div>
    </div>
  </div>

  <div class="col-md-3 col-sm-6 col-xs-12">
    <div class="info-box">
      <span class="info-box-icon bg-red"><i class="ion ion-cube"></i></span>
      <div class="info-box-content">
        <span class="info-box-text">Products</span>
        <span class="info-box-number">{{ $products }}</span>
      </div>
    </div>
  </div>

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>

  <div class="col-md-3 col-sm-6 col-xs-12">
    <div class="info-box">
      <span class="info-box-icon bg-green"><i class="ion ion-ios-cart-outline"></i></span>
      <div class="info-box-content">
        <span class="info-box-text">Sales</span>
        <span class="info-box-number">{{ $orders }}</span>
      </div>
    </div>
  </div>
        <!-- /.col -->
  <div class="col-md-3 col-sm-6 col-xs-12">
    <div class="info-box">
      <span class="info-box-icon bg-yellow"><i class="ion ion-ios-people-outline"></i></span>
      <div class="info-box-content">
        <span class="info-box-text">Members</span>
        <span class="info-box-number">{{ $users }}</span>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-6">
    <div class="box box-success">
      <div class="box-header with-border">
        <h3 class="box-title">Visitors and Page view</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
      </div>
      <div class="box-body">
        <div id="columnchart_material" style="width: 795; height: 500px;"></div>
      </div>
    </div>
    <div class="box box-secondary">
      <div class="box-header with-border">
        <h3 class="box-title">Most visited pages</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
      </div>
      <div class="box-body">
        <table class="table">
          <thead class="thead-light">
            <tr>
              <th scope="col">#</th>
              <th scope="col">Title</th>
              <th scope="col">Url</th>
              <th scope="col">Views</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($mostVisitedPages as $key => $value)
            <tr>
              <th scope="row">{{ $loop->iteration }}</th>
              <td>{{ $value['pageTitle'] }}</td>
              <td>{{ $value['url'] }}</td>
              <td>{{ $value['pageViews'] }}</td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
    <div class="box box-danger">
      <div class="box-header with-border">
        <h3 class="box-title">Browser Usage</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
      </div>
      <div class="box-body">
        <div id="donutchart" style="height: 403px; width: 795px;" width="795" height="403"></div>
      </div>
    </div>
  </div>
  <div class="col-md-6">
    <div class="box box-secondary">
      <div class="box-header with-border">
        <h3 class="box-title">Time on pages</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
      </div>
      <div class="box-body">
        <table class="table">
          <thead class="thead-light">
            <tr>
              <th scope="col">#</th>
              <th scope="col">Page (url))</th>
              <th scope="col">Time</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($timepages as $key => $value)
            <tr>
              <th scope="row">{{ $loop->iteration }}</th>
              <td>{{ $value['0'] }}</td>
              <td>{{ $value['1'] }}</td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Most visits by countries</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
      </div>
      <div class="box-body">
        <div id="regions_div"></div>
      </div>
    </div>
    <div class="box box-danger">
      <div class="box-header with-border">
        <h3 class="box-title">Operating Systems</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
      </div>
      <div class="box-body">
        <div id="OsChart" style="height: 403px; width: 795px;" width="795" height="403"></div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
    google.charts.load("current", {
      'packages': ['corechart', 'geochart', 'bar' ],
      'mapsApiKey': 'AIzaSyD-9tSrke72PouQMnMX-a7eZSW0jkFMBWY'
    });
    google.charts.setOnLoadCallback(drawChart);
    function drawChart() {
      var data = google.visualization.arrayToDataTable([
        ['Browsers', 'Sessions'],
        @foreach ($browsers as $key => $value)
          ['{{ $value['browser'] }}', {{ $value['sessions'] }}],
        @endforeach
      ]);

      var options = {
        is3D: true,
      };

      var chart = new google.visualization.PieChart(document.getElementById('donutchart'));
      chart.draw(data, options);
    }

     google.charts.setOnLoadCallback(drawRegionsMap);

     function drawRegionsMap() {
       var data = google.visualization.arrayToDataTable([
         ['Country', 'Visits'],
         @foreach ($countries as $key => $value)
           ['{{ $value['0'] }}', {{ $value['1'] }}],
         @endforeach
       ]);

       var options = {};

       var chart = new google.visualization.GeoChart(document.getElementById('regions_div'));

       chart.draw(data, options);
     }
     google.charts.setOnLoadCallback(drawBarChart);

      function drawBarChart() {
        var data = google.visualization.arrayToDataTable([
          ['Date', 'Page views', 'Visitors'],
          @foreach($visitspages as $key => $value)
            ['{{ $value['date'] }}', {{ $value['pageViews'] }}, {{ $value['visitors'] }}],
          @endforeach

        ]);

        var options = {};

        var chart = new google.charts.Bar(document.getElementById('columnchart_material'));

        chart.draw(data, google.charts.Bar.convertOptions(options));
      }
      google.charts.setOnLoadCallback(drawOsChart);
      function drawOsChart() {
        var data = google.visualization.arrayToDataTable([
          ['OS', 'Views'],
          @foreach ($operatingsystems as $key => $value)
            ['{{ $value['0'] }}', {{ $value['1'] }}],
          @endforeach
        ]);

        var options = {
          is3D: true,
        };

        var chart = new google.visualization.PieChart(document.getElementById('OsChart'));
        chart.draw(data, options);
      }
</script>
@stop
