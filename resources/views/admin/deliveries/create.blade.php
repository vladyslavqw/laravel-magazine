@extends('adminlte::page')

@section('title', 'Deliveries')

@section('content_header')
    <h1>Deliveries Management</h1>
@stop

@section('content')
<div class="box">
  <div class="box-header with-border">
    <div class="pull-left">
        <h3 class="box-title">Create new delivery</h3>
    </div>
    <div class="pull-right">
      <a href="{{route('deliveries.index')}}" class="btn btn-primary">Back</a>
    </div>
  </div>
  <div class="box-body">
    @include('errors.list')
    <form action="{{route('deliveries.store')}}" method="post">
      @csrf
      @include('admin.deliveries.__form')
    </form>

  </div>
</div>
@stop
