<div class="form-group mb-0" role="group">
  <a class="btn btn-primary" href="{{ route('deliveries.edit', $model->id)}}" role="button">Edit</a>
  <form method="POST" action="{{ route('deliveries.destroy', $model->id)}}" accept-charset="UTF-8" style="display:inline">
    @csrf
    @method('DELETE')
    <button class="btn btn-danger" type="submit">Delete</button>
  </form>
</div>
