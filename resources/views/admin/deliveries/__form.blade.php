<div class="form-group">
    <label for="title">Name</label>
    <input name="title" type="text" value="{{$deliveries->title ?? ''}}" id="name" class="form-control">
</div>

<div class="col-xs-12 col-sm-12 col-md-12 text-center">
    <button class="btn btn-primary" type="submit">Save</button>
</div>
