@extends('adminlte::page')
@section('title', 'Comments')

@section('content_header')
    <h1>Comments Management</h1>
@stop

@section('content')
<div class="box">
  <div class="box-header with-border">
    <div class="pull-left">
        <h3 class="box-title">Comment view</h3>
    </div>
    <div class="pull-right">
        <a class="btn btn-primary" href="{{ route('comments.edit', $comment) }}"> Edit</a>
        <a class="btn btn-primary" href="{{ route('comments.index') }}"> Back</a>
    </div>
  </div>
  <div class="box-body">
    <dl class="row">
      <dt class="col-sm-3">Name</dt>
      <dd class="col-sm-9">{{ $comment->name }}</dd>
      <dt class="col-sm-3">E-mail</dt>
      <dd class="col-sm-9">{{ $comment->email }}</dd>
      <dt class="col-sm-3">Product</dt>
      <dd class="col-sm-9">{{ $comment->product_id }}</dd>
    </dl>
    <div class="from-group">
      <label><strong>Comment</strong></label>
      <p> {{ $comment->body }} </p>
    </div>
  </div>
</div>
@stop
