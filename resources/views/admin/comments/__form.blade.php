<div class="form-group">
    <label for="name">Name</label>
    <input name="name" type="text" value="{{ $comment->name }}" id="name" class="form-control">
</div>
<div class="form-group">
    <label for="email">E-mail</label>
    <input name="email" type="text" value="{{ $comment->email }}" id="email" class="form-control">
</div>
<div class="form-group">
    <label for="body">Comment body</label>
    <textarea name="body" rows="8" cols="80" class="form-control">
      {{ $comment->body }}
    </textarea>
</div>
<div class="col-xs-12 col-sm-12 col-md-12 text-center">
    <button class="btn btn-primary" type="submit">Save</button>
</div>
