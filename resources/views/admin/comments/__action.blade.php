<div class="form-group mb-0" role="group">
  <a href="{{ route('comments.show', $model->id) }}" class="btn btn-success">Show</a>
  @can('comment-edit')
  <a class="btn btn-primary" href="{{ route('comments.edit', $model->id)}}" role="button">Edit</a>
  @endcan
  @can('comment-delete')
  <form method="POST" action="{{ route('comments.destroy', $model->id)}}" accept-charset="UTF-8" style="display:inline">
    @csrf
    @method('DELETE')
    <button class="btn btn-danger" type="submit">Delete</button>
  </form>
  @endcan
</div>
