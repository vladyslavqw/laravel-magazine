@extends('adminlte::page')

@section('title', 'Comments')

@section('content_header')
    <h1>Comments Management</h1>
@stop

@section('content')
<div class="box">
  <div class="box-header with-border">
    <div class="pull-left">
        <h3 class="box-title">Edit comment</h3>
    </div>
    <div class="pull-right">
      <a href="{{route('comments.index')}}" class="btn btn-primary">Back</a>
    </div>
  </div>
  <div class="box-body">
    @include('errors.list')
    <form action="{{route('comments.update', $comment->id)}}" method="post">
      @csrf
      @method('PUT')
      @include('admin.comments.__form')
    </form>
  </div>
</div>
@stop
