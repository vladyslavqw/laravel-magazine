@extends('adminlte::page')

@section('title', 'Roles')

@section('content_header')
    <h1>Roles Management</h1>
@stop

@section('content')
<div class="box">
  <div class="box-header with-border">
    <div class="pull-left">
        <h3 class="box-title">All roles</h3>
    </div>
    <div class="pull-right">
      <a href="{{route('roles.create')}}" class="btn btn-primary">Create</a>
    </div>
  </div>
  <div class="box-body">
    @include('messages.list')
    {!! $dataTable->table() !!}
  </div>
</div>
@stop

@section('js')
{!! $dataTable->scripts() !!}
<script src="https://cdn.datatables.net/buttons/1.0.3/js/dataTables.buttons.min.js"></script>
<script src="/vendor/datatables/buttons.server-side.js"></script>
@stop
