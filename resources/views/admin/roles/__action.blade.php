<div class="form-group mb-0" role="group">
  <a class="btn btn-success" href="{{ route('roles.show', $model->id)}}" role="button">Show</a>
  @can('role-edit')
  <a class="btn btn-primary" href="{{ route('roles.edit', $model->id)}}" role="button">Edit</a>
  @endcan
  @can('role-delete')
  <form method="POST" action="{{ route('roles.destroy', $model->id)}}" accept-charset="UTF-8" style="display:inline">
    @csrf
    @method('DELETE')
    <button class="btn btn-danger" type="submit">Delete</button>
  </form>
  @endcan
</div>
