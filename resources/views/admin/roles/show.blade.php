@extends('adminlte::page')

@section('title', 'Roles')

@section('content_header')
    <h1>Roles Management</h1>
@stop

@section('content')
<div class="box">
  <div class="box-header with-border">
    <div class="pull-left">
        <h3 class="box-title">{{ $role->name }} Role</h3>
    </div>
    <div class="pull-right">
        <a class="btn btn-primary" href="{{ route('roles.index') }}"> Back</a>
    </div>
  </div>

  <div class="box-body">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Name:</strong>
                {{ $role->name }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Permissions:</strong>
                @if(!empty($rolePermissions))
                    @foreach($rolePermissions as $permission)
                        <label class="badge badge-success">{{ $permission->name }}</label>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
  </div>

</div>
@stop
