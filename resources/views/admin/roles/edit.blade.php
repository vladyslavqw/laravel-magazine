@extends('adminlte::page')
@section('title', 'Roles')

@section('content_header')
    <h1>Roles Management</h1>
@stop

@section('content')
<div class="box">
  <div class="box-header with-border">
    <div class="pull-left">
        <h3 class="box-title">Edit role</h3>
    </div>
    <div class="pull-right">
        <a class="btn btn-primary" href="{{ route('roles.index') }}"> Back</a>
    </div>
  </div>
  <div class="box-body">
    @include('errors.list')
    <form action="{{route('roles.update', $role->id)}}" method="post">
      @csrf
      @method('PUT')
      @include('admin.roles.__form')
    </form>
  </div>
</div>


@stop
@section('js')
<script type="text/javascript">
  $(document).ready(function() {
    $('.permissions').select2();
  });
</script>
@stop
