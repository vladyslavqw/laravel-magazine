<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Name:</strong>
            <input placeholder="Name" class="form-control" name="name" type="text" value="{{$role->name ?? ''}}">
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
      <div class='form-group'>
        <label>Permissions:</label>
        <select class="form-control permissions" multiple="multiple"  name="permissions[]" data-placeholder="Select a permissions" style="width: 100%;" tabindex="-1" aria-hidden="true">
          @foreach ($permissions as $permission)
            <option value="{{$permission->id}}" @if( Route::current()->getName() == 'roles.edit' ) {{in_array($permission->id, $selectedPermissions)?'selected':''}} @endif>{{$permission->name}}</option>
          @endforeach
        </select>
      </div>

    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
        <button class="btn btn-primary" type="submit">Save</button>
    </div>
</div>
