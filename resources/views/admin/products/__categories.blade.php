@foreach($categories as $categoryItem)
  <option value="{{$categoryItem->id ?? ''}}"
    @isset($product->id)
      @if ($product->categories->contains('id', $categoryItem->id))
        selected=""
      @endif
    @endisset
  >
  {{ $delimiter ?? '' }} {{ $categoryItem->name ?? '' }}
</option>

  @isset($categoryItem->children)
    @include('admin.products.__categories', [
      'categories' => $categoryItem->children,
      'delimiter' => ' - ' . $delimiter
    ])
  @endisset
@endforeach
