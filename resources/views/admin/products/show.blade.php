@extends('adminlte::page')
@section('title', 'Products')

@section('content_header')
    <h1>Products Management</h1>
@stop

@section('content')
<div class="box">
  <div class="box-header with-border">
    <div class="pull-left">
        <h3 class="box-title">Product view</h3>
    </div>
    <div class="pull-right">
        <a class="btn btn-primary" href="{{ route('products.edit', $product) }}"> Edit</a>
        <a class="btn btn-primary" href="{{ route('products.index') }}"> Back</a>
    </div>
  </div>
  <div class="box-body">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <div class="row">
                    @foreach ($product->images as $image)
                        <div class="col-sm-4">
                            <a href="{{ asset('/storage/'.$image->path) }}" data-toggle="lightbox">
                                <img src="{{ asset('/storage/'.$image->path) }}" class="img-fluid">
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="col-sm-6">
                <div class="product-info">
                   <h3>{{ $product->title }}</h3>
                    <div class="product-categories mb-3">
                        @foreach ($product->categories as $category)
                            <span class="badge badge-success">{{ $category->name }}</span>
                        @endforeach
                    </div>
                    <div class="product-description">
                        <label class="mb-0"><strong>Description:</strong></label>
                        <p>{{ $product->description }}</p>    
                    </div>
                    <div class="product-deliveries">
                        <label class="mb-0"><strong>Deliveries:</strong></label>
                        @foreach ($product->deliveries as $delivery)
                            <p>{{ $delivery->title }}</p> 
                        @endforeach
                               
                    </div>    
                    <div class="row">
                        <div class="col">
                            <strong>SKU:</strong> {{ $product->sku }}
                        </div>
                        <div class="col">
                            <strong>Price:</strong> {{ $product->price }}
                        </div>
                        <div class="col">
                            <strong>Qty:</strong> {{ $product->qty }}
                        </div>
                        <div class="col">
                            <strong>Published:</strong> @if ($product->published) Yes @else No @endif
                        </div>
                    </div> 
                </div>
            </div>
        </div>
    </div>
  </div>
</div>

@stop
@section('css')
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.css">
@stop
@section('js')
<script src="//cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.min.js"></script>
<script>
$(document).on('click', '[data-toggle="lightbox"]', function(event) {
    event.preventDefault();
    $(this).ekkoLightbox();
});
</script>
@stop