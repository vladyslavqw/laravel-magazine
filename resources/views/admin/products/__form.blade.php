<div class="alert alert-info alert-dismissible">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
  <h5><i class="icon fa fa-info"></i> Alert!</h5>
  <p>For first, pick and upload the image first. Then save information about product.
Otherwise, images will not be assigned to the product.</p>
</div>

<div class="nav-tabs-product">
  <ul class="nav nav-tabs">
    <li class="nav-item"><a href="#tab_1" data-toggle="tab" aria-expanded="true" class="nav-link active">General info</a></li>
    <li class="nav-item"><a href="#tab_2" data-toggle="tab" aria-expanded="false" class="nav-link">Images</a></li>
  </ul>
  <div class="tab-content">
    <div class="tab-pane active" id="tab_1">
      <div class="form-group">
        <label for="sku">SKU</label>
        <input type="text" class="form-control" name="sku" value="{{ $product->sku ?? '' }}">
      </div>

      <div class="form-group">
        <label for="title">Product name</label>
        <input type="text" class="form-control" name="title" value="{{ $product->title ?? '' }}">
      </div>

      <div class="form-group">
        <label for="categories[]">Categories:</label>
        <select class="form-control product-categories" name="categories[]" multiple="">
          @include('admin.products.__categories')
        </select>
      </div>

      <div class="form-group">
        <label for="deliveries[]">Deliveries methods:</label>
        <select class="form-control product-categories" name="deliveries[]" multiple="">
          @include('admin.products.__deliveries')
        </select>
      </div>

      <div class="form-group">
        <label for="desctiption">Desctiption</label>
        <textarea class="form-control" name="description" rows="8" cols="80">
          {{ $product->description ?? '' }}
        </textarea>
      </div>

      <div class="form=group">
        <label for="qty">Quantity</label>
        <input type="number" class="form-control" name="qty" value="{{ $product->qty ?? '' }}">
      </div>

      <div class="form-group">
        <label for="price">Price</label>
        <input type="number" class="form-control" name="price" value="{{ $product->price ?? '' }}" step="any">
      </div>

      <div class="form-group">
        <label for="specs" class="form-check-label">Published</label>
        <select class="form-control" name="published">
          <option value="1">Yes</option>
          <option value="0">No</option>
        </select>
      </div>

      <div class="col-xs-12 col-sm-12 col-md-12 text-center">
          <button class="btn btn-primary" type="submit">Save</button>
      </div>

    </div>
    <div class="tab-pane" id="tab_2">
      <upload-file-component :files="{{ $product->images ?? '[]' }}"></upload-file-component>
    </div>
  </div>
</div>
