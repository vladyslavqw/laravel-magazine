@extends('adminlte::page')
@section('title', 'Products')

@section('content_header')
    <h1>Products Management</h1>
@stop

@section('content')
<div class="box">
  <div class="box-header with-border">
    <div class="pull-left">
        <h3 class="box-title">Edit product</h3>
    </div>
    <div class="pull-right">
        <a class="btn btn-primary" href="{{ route('products.index') }}"> Back</a>
    </div>
  </div>
  <div class="box-body">
    @include('errors.list')
    <form action="{{route('products.update', $product)}}" method="post">
      @csrf
      @method('PUT')
      @include('admin.products.__form')
    </form>
  </div>
</div>

@stop

@section('js')
<script type="text/javascript">
  $(document).ready(function() {
    $('.product-categories').select2();
  });
</script>
@stop
