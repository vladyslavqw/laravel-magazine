<div class="form-group mb-0" role="group">
  <a href="{{ route('products.show', $model->id) }}" class="btn btn-success">Show</a>
  @can('product-edit')
  <a class="btn btn-primary" href="{{ route('products.edit', $model->id)}}" role="button">Edit</a>
  @endcan
  @can('product-delete')
  <form method="POST" action="{{ route('products.destroy', $model->id)}}" accept-charset="UTF-8" style="display:inline">
    @csrf
    @method('DELETE')
    <button class="btn btn-danger" type="submit">Delete</button>
  </form>
  @endcan
</div>
