@foreach($deliveries as $delivery)
  <option value="{{$delivery->id ?? ''}}"
    @isset($product->id)
      @if ($product->deliveries->contains('id', $delivery->id))
        selected=""
      @endif
    @endisset
  >
  {{ $delivery->title ?? '' }}
</option>
@endforeach