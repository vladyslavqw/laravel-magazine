@foreach($categories as $category)
  @if($category->children->count())
  <li class="nav-item dropdown">
    <a class="nav-link dropdown-toggle" href="{{route('category.menu', $category->slug)}}" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      {{ $category->name ?? '' }}
    </a>
    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
      @include('layouts.__menu', ['categories' => $category->children, 'is_child' => true])

    </div>
  </li>
  @else
    @isset($is_child)
      <a class="dropdown-item" href="{{route('category.menu', $category->slug)}}">{{ $category->name ?? '' }}</a>
      @continue
    @endisset
    <li class="nav-item active">
      <a class="nav-link" href="{{route('category.menu', $category->slug)}}">{{ $category->name ?? '' }}</a>
    </li>
  @endif
@endforeach
