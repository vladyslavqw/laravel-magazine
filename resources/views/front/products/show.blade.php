@extends('layouts.app')

@section('content')
<div class="container">
  @include('messages.list')
  <div class="row mb-5">
    <div class="col-md-6">
      <div class="row mb-4">
        <div class="col-sm-12">
          <a  data-fancybox="gallery" href="{{ asset('/storage/'.$product->images()->first()->path) }}">
              <img src="{{ asset('/storage/'.$product->images()->first()->path) }}" alt="" class="img-fluid">
          </a>
        </div>
      </div>
      <div class="row">
        @foreach ($product->images as $image)
        <div class="col-md-4">
          <a  data-fancybox="gallery" href="{{ asset('/storage/'.$image->path) }}">
            <img src="{{ asset('/storage/'.$image->path) }}" class="img-fluid">
          </a>
        </div>
        @endforeach
      </div>
    </div>
    <div class="col-sm-6">
        <div class="product-info">
           <h3>{{ $product->title }}</h3>
           <hr>
           <h4>$ {{ $product->price }}</h4>
           <br>
            <div class="product-description">
                <p>{{ mb_substr($product->description, 0, 120) }}..</p>
            </div>
            <div class="product-deliveries">
                <label class="mb-0"><strong>Deliveries:</strong></label>
                @foreach ($product->deliveries as $delivery)
                    <p>{{ $delivery->title }}</p>
                @endforeach

            </div>
            <hr>
            <div class="row">
                <div class="col">
                    <strong>SKU:</strong> {{ $product->sku }}
                </div>
                <div class="col">
                    <strong>Qty:</strong> {{ $product->qty }}
                </div>
            </div>
            <hr>
            <button class="btn btn-lg btn-success addToCart" data-pid="{{$product->id}}">Add to cart</button>
        </div>
    </div>
  </div>
  @include('errors.list')
  <div class="row">
    <div class="col-sm-12">
      <ul class="nav nav-tabs" id="myTab" role="tablist">
        <li class="nav-item">
          <a class="nav-link active" id="desc-tab" data-toggle="tab" href="#desc" role="tab" aria-controls="desc" aria-selected="true">Description</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" id="profile-tab" data-toggle="tab" href="#comments" role="tab" aria-controls="comments" aria-selected="false">Comments</a>
        </li>
      </ul>
      <div class="tab-content">
        <div class="tab-pane fade show active" id="desc" role="tabpanel" aria-labelledby="desc-tab">
          <p class="p-3">{{ $product->description }}</p>
        </div>
        <div class="tab-pane fade" id="comments" role="tabpanel" aria-labelledby="comments-tab">
          <div class="row">
            <div class="col p-3">
              <form class="" action="{{ route('comments.store') }}" method="post">
                @csrf
                @guest
                <div class="form-group">
                  <label for="email">Email address</label>
                  <input type="email" class="form-control" name="email" aria-describedby="emailHelp" placeholder="Enter email">
                  <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                </div>
                <div class="form-group">
                  <label for="name">Name</label>
                  <input type="text" class="form-control" name="name" placeholder="Enter name">
                </div>
                @endguest
                @auth
                  <input type="hidden" name="name" value="{{ Auth::user()->name }}">
                  <input type="hidden" name="email" value="{{ Auth::user()->email }}">
                @endauth
                <div class="form-group">
                  <label for="body">Comment</label>
                  <textarea name="body" rows="8" cols="80" class="form-control" placeholder="enter your comment"></textarea>
                </div>
                <input type="hidden" name="product_id" value="{{ $product->id }}">
                <div class="col-sm-12 text-center">
                  <button type="submit" class="btn btn-primary">Add</button>
                </div>
              </form>
            </div>
            @foreach($comments as $comment)
            <div class="col-sm-12 mt-3 mb-3">
              <div class="card">
                <div class="card-header">
                  {{ $comment->name }}
                </div>
                <div class="card=body p-3">
                  {{ $comment->body }}
                </div>
              </div>
            </div>
            @endforeach
            {{ $comments->links() }}
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('css')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
@endsection
@section('js')
<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
@endsection
