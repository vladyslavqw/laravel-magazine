@extends('layouts.app')

@section('content')
    <div class="container">
      <div class="py-5 text-center">
        <h2>Checkout form</h2>
      </div>
      @include('errors.list')
      <div class="row">
        <div class="col-md-4 order-md-2 mb-4">
          <h4 class="d-flex justify-content-between align-items-center mb-3">
            <span class="text-muted">Your cart</span>
            <span class="badge badge-secondary badge-pill">{{Cart::count()}}</span>
          </h4>
          <ul class="list-group mb-3">
            @foreach (Cart::content() as $cartItem)
            <li class="list-group-item d-flex justify-content-between lh-condensed">
              <div>
                <h6 class="my-0">{{$cartItem->name}}</h6>
              </div>
              <span class="text-muted">{{$cartItem->price}}</span>
            </li>
            @endforeach

            <li class="list-group-item d-flex justify-content-between">
              <span>Total (USD)</span>
              <strong>${{Cart::total()}}</strong>
            </li>
          </ul>
        </div>
        <div class="col-md-8 order-md-1">
          <h4 class="mb-3">Billing address</h4>
          <form class="needs-validation" novalidate action="{{route('checkout.store')}}" method="post" id="payment-form">
            @csrf
            <div class="row">

            <div class="col-12 mb-3 p-0">
              <label for="name">Name</label>
              <input type="text" class="form-control" id="name" name="name" value="{{ $user->profiles->surename ?? '' }}" required>
              <div class="invalid-feedback" style="width: 100%;">
                Your name is required.
              </div>
            </div>

            <div class="col-12 mb-3 p-0">
              <label for="email">Email</label>
              <input type="email" class="form-control" id="email" name="email" value="{{ $user->email ?? '' }}" placeholder="you@example.com">
              <div class="invalid-feedback">
                Please enter a valid email address for shipping updates.
              </div>
            </div>

            <div class="col-12 mb-3 p-0">
              <label for="address">Address</label>
              <input type="text" class="form-control" id="address" name="address" value="{{ $user->profiles->address ?? '' }}" placeholder="1234 Main St" required>
              <div class="invalid-feedback">
                Please enter your shipping address.
              </div>
            </div>

            <div class="row">
              <div class="col-md-5 mb-3">
                <label for="city">Phone</label>
                <input type="text" class="form-control" id="city" name="phone" value="{{ $user->profiles->phone ?? '' }}" required>
                <div class="invalid-feedback">
                  Please select a valid phone.
                </div>
              </div>
              <div class="col-md-2 mb-3">
                <label for="city">City</label>
                <input type="text" class="form-control" id="city" name="city" value="{{ $user->profiles->city ?? '' }}" required>
                <div class="invalid-feedback">
                  Please select a valid city.
                </div>
              </div>
              <div class="col-md-3 mb-3">
                <label for="province">Province</label>
                <input type="text" class="form-control" id="province" name="province" value="{{ $user->profiles->province ?? '' }}" required>
                <div class="invalid-feedback">
                  Please provide a valid state.
                </div>
              </div>
              <div class="col-md-2 mb-3">
                <label for="zip">Zip</label>
                <input type="text" class="form-control" id="zip" name="postalcode" value="{{ $user->profiles->postalcode ?? '' }}" required>
                <div class="invalid-feedback">
                  Zip code required.
                </div>
              </div>
            </div>
          </div>
            <hr class="mb-4">

            <h4 class="mb-3">Payment</h4>

            <div class="row">
              <div class="col-md-6 mb-3">
                <label for="cc-name">Name on card</label>
                <input type="text" class="form-control" id="cc-name" name="card-holder" placeholder="" required>
                <small class="text-muted">Full name as displayed on card</small>
                <div class="invalid-feedback">
                  Name on card is required
                </div>
              </div>
              <div class="col-md-6 mb-3">
                <label for="cc-number">Credit card number</label>
                <input type="text" class="form-control" id="cc-number" name="card-number" placeholder="" required>
                <div class="invalid-feedback">
                  Credit card number is required
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-3 mb-3">
                <label for="cc-expiration">Expiration month</label>
                <input type="text" class="form-control" id="cc-expiration" name="exp_mm" placeholder="mm" required>
                <div class="invalid-feedback">
                  Expiration date required
                </div>
              </div>
              <div class="col-md-3 mb-3">
                <label for="cc-expiration">Expiration year</label>
                <input type="text" class="form-control" id="cc-expiration" name="exp_yy" placeholder="yy" required>
                <div class="invalid-feedback">
                  Expiration date required
                </div>
              </div>
              <div class="col-md-3 mb-3">
                <label for="cc-expiration">CVV</label>
                <input type="text" class="form-control" id="cc-cvv" name="cvc" required>
                <div class="invalid-feedback">
                  Security code required
                </div>
              </div>
            </div>
            <hr class="mb-4">
            <button class="btn btn-primary btn-lg btn-block" type="submit">Continue to checkout</button>
          </form>
        </div>
      </div>
@endsection

@section('css')
<link rel="stylesheet" href="{{asset('css/payment.css')}}">
@endsection
