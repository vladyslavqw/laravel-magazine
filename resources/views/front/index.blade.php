@extends('layouts.app')

@section('content')

  <div class="container">
    <div class="row">
      @foreach ($products as $product)
      <div class="col-3 mb-3">
        <div class="card">
          @foreach ($product->images as $item)
          <img class="card-img-top" src="/storage/{{ $item->path}}" alt="{{$item->title}}">
            @break
          @endforeach
          <div class="card-body">
            <h5 class="card-title">{{ $product->title }}</h5>
            <h6>${{ $product->price }}</h6>
            <p class="card-text">{{ mb_substr($product->description, 0, 40) }}</p>
            <button class="btn btn-secondary addToCart" data-pid="{{$product->id}}">Add to cart</button>
            <a href="/p/{{ $product->slug }}" class="btn btn-primary">View</a>
          </div>
        </div>
      </div>
      @endforeach
    </div>
    {{ $products->links() }}
  </div>
  @endsection
