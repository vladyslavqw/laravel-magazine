@extends('layouts.app')

@section('content')
<section class="shopping-cart dark">
  <div class="container">
        <div class="block-heading">
          <h2>Shopping Cart</h2>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc quam urna, dignissim nec auctor in, mattis vitae leo.</p>
        </div>
        <div class="content">
      <div class="row">
        <div class="col-md-12 col-lg-7">
          <div class="items">
           @if (sizeof(Cart::content()) > 0)
            @foreach (Cart::content() as $cartItem)
            <div class="product">
              <div class="row">
                  <div class="col-md-12 ml-5">
                  <div class="info">
                    <div class="row">
                      <div class="col-md-4 product-name align-self-center">
                        <div class="product-name">
                          <a href="#">{{$cartItem->name}}</a>
                        </div>
                      </div>
                      <div class="col-md-3 quantity-box">
                        <label for="quantity">Quantity:</label>
                        <input id="quantity" type="number" value ="{{$cartItem->qty}}" class="form-control quantity quantity-input" data-id="{{$cartItem->rowId}}">
                      </div>
                      <div class="col-md-2 price">
                        <span>${{$cartItem->price}}</span>
                      </div>
                      <div class="col-md-1 align-self-center">
                        <button type="button" class="close delete" data-rowId="{{$cartItem->rowId}}"><i class="far fa-trash-alt"></i></button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            @endforeach
            @else
              <div class="row">
                <div class="col-sm-12 text-center">
                  <h3>You have no items in your shopping cart</h3>
                  <a href="{{ url('/') }}" class="btn btn-primary mt-3">Continue Shopping</a>
                </div>
              </div>
            @endif
          </div>
        </div>
        <div class="col-md-12 col-lg-5">
          <div class="summary">
            <h3>Summary</h3>
            <div class="summary-item"><span class="text">Subtotal</span><span class="price">${{Cart::subtotal()}}</span></div>
            <div class="summary-item"><span class="text">Discount</span><span class="price">$0</span></div>
            <div class="summary-item"><span class="text">Tax</span><span class="price">${{Cart::tax()}}</span></div>
            <div class="summary-item"><span class="text">Total</span><span class="price">${{Cart::total()}}</span></div>
            <a href="{{route('checkout')}}" class="btn btn-primary btn-lg btn-block mt-5">Checkout</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection

@section('js')
<script>
$(document).ready( function () {
  const classname = document.querySelectorAll('.quantity')
  Array.from(classname).forEach(function(element) {
      element.addEventListener('change', function() {
          const id = element.getAttribute('data-id')
          axios.patch(`/cart/update/${id}`, {
              quantity: this.value
          })
          .then(function (response) {
              // console.log(response);
              window.location.href = '{{ route('cart.index') }}'
          })
          .catch(function (error) {
              // console.log(error);
              window.location.href = '{{ route('cart.index') }}'
          });
      })
  })
  const deleterow = document.querySelectorAll('.delete')
  Array.from(deleterow).forEach(function(element) {
      element.addEventListener('click', function() {
          const id = element.getAttribute('data-rowId')
          axios.delete(`/cart/delete/${id}`)
          .then(function (response) {
              // console.log(response);
              window.location.href = '{{ route('cart.index') }}'
          })
          .catch(function (error) {
              // console.log(error);
              window.location.href = '{{ route('cart.index') }}'
          });
      })
  })
});

</script>
@endsection

@section('css')
<link rel="stylesheet" href="{{asset('css/shopping-cart.css')}}">
@endsection
